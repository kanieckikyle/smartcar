# Smart Car GM API Facade
Smart car's Generic Motors API Facade to make dealing with their API a little easier.

### Setup
To run the server, I would highly recommend using virtualenv. This allows this server's dependencies to be 
sandboxed from your machine's global python dependencies. Imagine npm on steroids.

If on mac, I would recommend brew to install virtualenv
`brew install virtualenv`

If on Ubuntu, you can grab it right from apt
`sudo apt install virtualenv`

From here, navigate into the smartcar directory and run
`virtualenv -p $(which python3) ./env`
** Note, we specify which python we want to use just in case virtualenv decides to use python2 for some reason

Now, activate the virtualenv using
`source ./env/bin/activate`
Once you have done this, your prompt should look similar to `(env) ➜  smartcar git:(master) ✗`
** Note: The example above is using zsh, but the important part is the `(env)` portion

### Installing Dependencies
To install the dependencies into your virtualenv, simple run `pip install -r requirements.txt`. You should start
to see pip install the needed packages.

### Running the Server
To run the flask server, we need to tell flask where the entrypoint for the server is. So we will export an environment
variable to let it know which script is the entrypoint.
`export FLASK_APP=server.py`

Once we have exported the variable, simple run `flask run` while in the virtualenv to start the server.

### Running Tests
To run the automated API tests, simple type `pytest` in your terminal while in the top level smartcar directory and pytest will autodiscover
the tests and run them automatically!

### Deactivating virtenv
To deactivate the virtualenv, simply type `deactivate` in your terminal
