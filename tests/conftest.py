import pytest
from ..server import app as flask_app

@pytest.fixture
def client():
    client = flask_app.test_client()
    yield client
