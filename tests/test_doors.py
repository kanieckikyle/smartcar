import pytest
from .constants import vehicle_ids, non_existent_vehicles
from .helpers import get_response_dict

@pytest.mark.parametrize('vehicle_id', vehicle_ids)
def test_success_response(client, vehicle_id):
    response = get_response_dict(client.get('/vehicles/{}/doors'.format(vehicle_id)))
    assert isinstance(response, list)

@pytest.mark.parametrize('vehicle_id', non_existent_vehicles)
def test_nonexistent_vehicle(client, vehicle_id):
    response = get_response_dict(client.get('/vehicles/{}/doors'.format(vehicle_id)))
    assert 'error' in response

@pytest.mark.parametrize('vehicle_id', vehicle_ids)
def test_make_sure_expected_values(client, vehicle_id):
    response = get_response_dict(client.get('/vehicles/{}/doors'.format(vehicle_id)))
    assert len(response) == 2 or len(response) == 4

    if len(response) == 4:
        doors = ['frontLeft', 'frontRight', 'backLeft', 'backRight']
    elif len(response) == 2:
        doors = ['frontLeft', 'frontRight']
    
    for door in response:
        if door['location'] in doors:
            doors.remove(door['location'])
    
    assert len(doors) == 0

@pytest.mark.parametrize('vehicle_id', vehicle_ids)
def test_correct_success_response_code(client, vehicle_id):
    response = client.get('/vehicles/{}/doors'.format(vehicle_id))
    assert response.status_code == 200

@pytest.mark.parametrize('vehicle_id', non_existent_vehicles)
def test_vehicle_not_found_response_code(client, vehicle_id):
    response = client.get('/vehicles/{}/doors'.format(vehicle_id))
    assert response.status_code == 404