import pytest
from .constants import fuel_vehicles, battery_vehicles, non_existent_vehicles
from .helpers import get_response_dict

@pytest.mark.parametrize('vehicle_id', fuel_vehicles)
def test_success_response(client, vehicle_id):
    response = get_response_dict(client.get('/vehicles/{}/fuel'.format(vehicle_id)))
    assert 'percent' in response

@pytest.mark.parametrize('vehicle_id', fuel_vehicles)
def test_correct_type(client, vehicle_id):
    response = get_response_dict(client.get('/vehicles/{}/fuel'.format(vehicle_id)))
    assert isinstance(response['percent'], float) or isinstance(response['percent'], int)

@pytest.mark.parametrize('vehicle_id', battery_vehicles)
def test_battery_vehicle_is_none(client, vehicle_id):
    response = get_response_dict(client.get('/vehicles/{}/fuel'.format(vehicle_id)))
    assert response['percent'] == None

@pytest.mark.parametrize('vehicle_id', fuel_vehicles)
def test_correct_success_response_code(client, vehicle_id):
    response = client.get('/vehicles/{}/fuel'.format(vehicle_id))
    assert response.status_code == 200

@pytest.mark.parametrize('vehicle_id', non_existent_vehicles)
def test_vehicle_not_found_response_code(client, vehicle_id):
    response = client.get('/vehicles/{}/fuel'.format(vehicle_id))
    assert response.status_code == 404
