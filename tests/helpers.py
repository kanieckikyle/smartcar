import json


def get_response_dict(response):
    return json.loads(response.data.decode())
