from requests import Session
from .exceptions import Http404Error

class GmApiService(object):
    """
    Class that interacts with the legacy GM API and makes it more manageable
    """

    # The base url of the GM API
    base_url = 'http://gmapi.azurewebsites.net'

    def __init__(self, vehicle_id: int):
        self.vehicle_id = str(vehicle_id)
        self.session = Session()
        self.session.headers.update({'Content-Type': 'application/json'})
    
    def get_vehicle_info(self):
        """
        Gets the vehicles information from the GM API

        :return: dict - the dict of vehicle information
        """
        response = self._call_api('getVehicleInfoService')

        formatted = {}
        for key, value in response['data'].items():
            formatted[key] = self._get_attr_value(value)
        
        return formatted
    
    def get_vehicle_security(self):
        """
        Gets the current status of all of the vehicle's doors

        :return: list - a list of door dictionaries that include the door location and the lock status
        """
        response = self._call_api('getSecurityStatusService')

        formatted = []

        # Go through all of the doors
        for door in response['data']['doors']['values']:
            # Get our new, better door blob
            better_door = {}
            for key, value in door.items():
                # Grab the type and value from the door
                better_door[key] = self._get_attr_value(value)
            formatted.append(better_door)

        return formatted
    
    def get_vehicle_fuel_status(self):
        """
        Gets the current fuel status for the vehicle from the GM API
        """
        response = self._call_api('getEnergyService')

        formatted = {}
        for key, value in response['data'].items():
            formatted[key] = self._get_attr_value(value)
        
        return formatted
    
    def start_engine(self):
        """
        Starts the engine of the vehicle

        :return: bool - True if the engine was started successfully, False if an error occurred
        """
        response = self._call_api('actionEngineService', {'command': 'START_VEHICLE'})
        return response['actionResult']['status'] == 'EXECUTED'
    
    def stop_engine(self):
        """
        Stops the engine of the vehicle

        :return: bool - True if the engine was stopped, False if an error occurred
        """
        response = self._call_api('actionEngineService', {'command': 'STOP_VEHICLE'})
        return response['actionResult']['status'] == 'EXECUTED'
    
    def _get_attr_value(self, blob: dict):
        """
        GM's API returns values as a blob with both the value and the type as descriptors
        This function consolidates that blob into a single native python value

        :param blob: The GM blob that contains the value and the type
        :return: str | bool | int | float | None
        """
        if blob['type'] == 'String':
            return str(blob['value'])
        elif blob['type'] == 'Boolean':
            return blob['value'] == 'True'
        elif blob['type'] == 'Number':
            try:
                return int(blob['value'])
            except ValueError:
                return float(blob['value'])
        
        return None
    
    def _call_api(self, endpoint, data={}):
        """
        Calls the GM API
        If the response code is in the 200s, then we return the json response
        Otherwise, we throw a Http404Error
        """
        data.update({'id': self.vehicle_id, 'responseType': 'JSON'})
        response = self.session.post('{}/{}'.format(self.base_url, endpoint), json=data)

        response = response.json()
        status_code = int(response.get('status', '400'))

        if status_code < 300 and status_code >= 200:
            return response

        raise Http404Error(response['reason'])
