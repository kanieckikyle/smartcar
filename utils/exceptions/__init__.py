from .http_404 import Http404Error

__all__ = [
    'Http404Error'
]
