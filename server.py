#!/usr/bin/env python

from flask import Flask, request, make_response
from .utils import GmApiService
from .utils.exceptions import Http404Error
import json
import werkzeug

app = Flask('Smartcar')

@app.errorhandler(Http404Error)
def handle_bad_request(e):
    """ Error handler to send back a 404 whenever my custom Http404 error occurs """
    return json.dumps({'error': str(e)}), 404

@app.route('/vehicles/<int:vehicle_id>', methods=['GET'])
def get_vehicle_info(vehicle_id):
    """
    Gets a vehicles basic information.
    Example blob looks as follows:
    {"vin": "123123412412", "color": "Metallic Silver", "driveTrain": "v8", "doorCount": 4}
    """
    service = GmApiService(vehicle_id)
    info = service.get_vehicle_info()

    info['doorCount'] = 4 if info['fourDoorSedan'] else 2

    for k in ['fourDoorSedan', 'twoDoorCoupe']:
        info.pop(k, None)
    
    return make_response((json.dumps(info), 200))

@app.route('/vehicles/<int:vehicle_id>/doors', methods=['GET'])
def get_vehicle_security(vehicle_id):
    """
    Gets the current status of the security of the vehicle. This includes all of the lock
    status for the car's doors
    An example return blob looks as follows:
    [
        {"location": "frontRight", "locked": true},
        {"location": "backLeft", "locked": true},
        {"location": "backRight", "locked": false},
        {"location": "frontLeft", "locked": false}
    ]
    """
    service = GmApiService(vehicle_id)
    return json.dumps(service.get_vehicle_security())

@app.route('/vehicles/<int:vehicle_id>/fuel', methods=['GET'])
def get_vehicle_fuel_info(vehicle_id):
    """
    Gets the current fuel level for the vehicle, given the vehicle id
    Returns the level in percent
    {
        'percent': 50.0
    }
    """
    service = GmApiService(vehicle_id)
    info = service.get_vehicle_fuel_status()
    
    info['percent'] = info.pop('tankLevel')
    info.pop('batteryLevel', None)

    return json.dumps(info)

@app.route('/vehicles/<int:vehicle_id>/battery', methods=['GET'])
def get_vehicle_battery_info(vehicle_id):
    """
    Gets the current battery level for a vehicle, give the vehicle id
    Returns the level in percent
    {
        'percent': 50.0
    }
    """
    service = GmApiService(vehicle_id)
    info = service.get_vehicle_fuel_status()
    
    info['percent'] = info.pop('batteryLevel')
    info.pop('tankLevel', None)
    
    return json.dumps(info)

@app.route('/vehicles/<int:vehicle_id>/engine', methods=['POST'])
def modify_engine_state(vehicle_id):
    """
    Start or stop a vehicle's engine
    """
    service = GmApiService(vehicle_id)
    action = request.form.get('action', None)
    success = False

    if action == 'START':
        success = service.start_engine()
    elif action == 'STOP':
        success = service.stop_engine()
    
    return json.dumps({'status': 'success' if success else 'error'})

if __name__ == '__main__':
    app.run()
